#pragma strict


public static var instance:GUIManager;
public var customSkin : GUISkin;

private var maxHP:float = 100.0;
private var p1HP:float;
private var p2HP:float;
private var p1Score:float;
private var p2Score:float;
private var timer:float;
private var timeGo:boolean;
//public var pTurn:boolean; //false == p1 true === p2

public function getMax():float { return maxHP;}
public function setp1HP(hp:float):void { p1HP = hp;}
public function getp1HP():float { return p1HP;}
public function setp2HP(hp:float):void { p2HP = hp;}
public function getp2HP():float { return p2HP;}
public function setp2Score(sc:float):void { p2Score = sc;}
public function getp2Score():float { return p2Score;}
public function setp1Score(sc:float):void { p1Score = sc;}
public function getp1Score():float { return p1Score;}


	function Start()
	{
		timeGo = true;
		timer = 3;
		print("Loaded: " + gamestate.Instance().getLevel());
		
	}
	
	public static function Instance():GUIManager
	{
			return instance;
	}
	
	function Awake() 
	{
		if (instance != null && instance != this)
			{
            Destroy(this);
            return;
        	}
        instance = this;
        DontDestroyOnLoad(this);
	}

	public function OnApplicationQuit():void
	{
		instance = null;
	}

	function OnGUI()
	{
	GUI.skin = customSkin;
	}
