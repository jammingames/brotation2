#pragma strict

/*
The physics has been abstracted into CPhysics class,
that handles how things move, we call that class's methods
 inside of this script to move the character, so we can also
 use it to move platforms for the kirby monster etc 
 */



/* -- Components -- */
private var playerRigidbody : Rigidbody;
private var playerSprite : tk2dAnimatedSprite;

/* -- Physics Properties -- */
//var gravity : float = 1.0;
private var HP : float = 100.0;
private var wallFriction : float = -0.79;
var wallGravity : float = -1.5;
var playerAcceleration : float = 1;
var speed : float = 20;
var playerFriction : Vector3 = Vector3(0.01,1,1);
var playerJumpFriction : Vector3 = Vector3(1,0.90,1);
private var directionVector:Vector3;
private var playerJump : float = 20; 
private var isOnWall : int;
private var landed = 0;
private var canMove :boolean;
private var wjump : int = 3;
private var movementThisStep : Vector3;
private var pPhys :CPhysics;
private var side : String;
/* -- Raycast Setup -- */
var platformLayer : LayerMask;
var wallLayer : LayerMask;
private var minimumSize : float;
private var partialExtent : float;
private var sqrMinimumSize : float;
private var previousPosition : Vector3;



function Start () 
{
	/* -- Initilize Raycast stuff -- */
	playerJump = 20;
	state =  SM.getState();
	playerRigidbody = rigidbody;
	previousPosition = playerRigidbody.position;
	minimumSize = Mathf.Min(collider.bounds.extents.x, collider.bounds.extents.y);
	partialExtent = minimumSize;
	sqrMinimumSize = minimumSize*minimumSize;
	canMove = true;
//	playerSprite = GetComponent(tk2dAnimatedSprite);
//	playerSprite.animationEventDelegate = AnimationEventDelegate;
	pPhys = new CPhysics(transform, platformLayer);
	
}
/* State Machine Setup */
enum ops {opDefault, opNext, opContinue, opRepeat};
enum states {stDefault, idle, onPlatform, jumping, falling, stNone, wallSliding, kickOffWall, disabled};

private var stateTable = new Hashtable();

stateTable.Add(states.stDefault, idle);
stateTable.Add(states.idle , idle);
stateTable.Add(states.onPlatform , onPlatform);
stateTable.Add(states.jumping, jumping);
stateTable.Add(states.falling, falling);
stateTable.Add(states.wallSliding, wallSliding);
stateTable.Add(states.kickOffWall, kickOffWall);
stateTable.Add(states.disabled, disabled);

private var SM : StateMachine = new StateMachine(ops.opDefault, stateTable);

//MUTHAFUCKIN STATES
//------------------------------------------------------------------------------------------------
function idle()
{
 	if (SM.newState() ) {
		//Debug.Log("idle!");
	}
	if (checkLanded()) {
		//Debug.Log("COLLIDE!");
		SM.setState(states.onPlatform);
 		return(ops.opNext);
	}
	return(ops.opContinue);
}

function onPlatform()
{ 
	wjump = 3;
	if (SM.newState() ) {
		//Debug.Log("onPlatform!");
		//playerSprite.Play("walkRight");
		canMove=true;
	}
	//if (Input.GetButtonDown("Jump")) {SM.setState(states.jumping); return(ops.opNext);}
	
	if (SM.getInterrupt(states.jumping)) {
		//Debug.Log("Got Jump Signal!");
		SM.setState(states.jumping);
		SM.clearInterrupt();
		landed = 0;
 		return(ops.opNext);
	}
		if (SM.getInterrupt(states.disabled)) {
		//Debug.Log("got disable signal");
		SM.setState(states.disabled);
		SM.clearInterrupt();
		return(ops.opNext);
	}
	if (checkLanded() && SM.newState == false) {
		SM.setState(states.jumping);
		return(ops.opNext);
		}
	
	//canMove = false;
	return(ops.opContinue);
}

function jumping()
{
	if (SM.newState() ) {
		//Debug.Log("jumping!");
	//	playerSprite.Play("jump");
		canMove=false;
	}
	if (SM.newState() && checkLanded()) {
		jump();
		//Debug.Log("----");
	}
	if (SM.newState() == false && checkFalling()) {
		//Debug.Log("APEX!");
		SM.setState(states.falling);
 		return(ops.opNext);
	}
	if (SM.getInterrupt(states.disabled)) {
		//Debug.Log("got disable signal");
		SM.setState(states.disabled);
		SM.clearInterrupt();
		return(ops.opNext);
	}

	//pPhys.doFriction(playerJumpFriction);
	return(ops.opContinue);
}

function falling()
{
	if (SM.newState() ) {
		//Debug.Log("FALLING!");
	//	playerSprite.Play("falling");
		landed = 0;
		canMove=false;
	}
	if (SM.getInterrupt(states.disabled)) {
		//Debug.Log("got disable signal");
		SM.setState(states.disabled);
		SM.clearInterrupt();
		return(ops.opNext);
	}
	if (checkLanded()) {
		//Debug.Log("LANDED!");
	//	playerSprite.Play("landed");
		SM.setState(states.onPlatform);
 		return(ops.opNext);
	}
	
	if (checkWalls()) {
		SM.setState(states.wallSliding);
		return(ops.opNext);
	}

	return(ops.opContinue);
}

function wallSliding()
{
	if (SM.newState() ) 
	{
		//Debug.Log("Sliding");
		//playerSprite.Play("wallSlide");
		canMove=false;
		playerSprite.scale.x = isOnWall;
	}
	
//	pPhys.motionSetY(wallGravity);
	
	if (playerRigidbody.velocity.x != 0) 
	{
		playerRigidbody.velocity.x = 0;
	}
	
	if (isOnWall == -1) 
	{
		if (wjump >= 0) {
		//	pPhys.motionAdd(Vector3(playerJump/3,playerJump,0));
			//Debug.Log("Walljump!");
			wjump--; 
			isOnWall = 0;
			}
		else
		{
		//	pPhys.motionAdd(Vector3(playerJump/3,0,0));
			wjump--;
		}
		playerSprite.scale = Vector3(1,1,1);
		SM.setState(states.kickOffWall);
		return(ops.opNext);
	}
	
	if (isOnWall == 1) 
	{
		if (wjump >= 3) {
			//pPhys.motionAdd(Vector3(-playerJump/3,playerJump,0));
			//Debug.Log("Walljump!");
			isOnWall = 0;
			wjump--;
			}
		else 
		{
			//pPhys.motionAdd(Vector3(-playerJump/3,0,0));
			wjump--;
		}
		playerSprite.scale = Vector3(-1,1,1);
		SM.setState(states.kickOffWall);
		return(ops.opNext);
	}
	
	
	if (checkLanded()) {
		SM.setState(states.onPlatform);
		return(ops.opNext);
	}
	
	
	return (ops.opContinue);
}

function kickOffWall()
{
if (SM.newState() ) {
		//Debug.Log("jumping off wall!");
		//playerSprite.Play("wallJump");
		canMove=false;
	}
	if (SM.getInterrupt(states.disabled)) {
		//Debug.Log("disabled");
		SM.clearInterrupt();
		SM.setState(states.disabled);
		landed = 0;
 		return(ops.opNext);
 	}
	if (SM.getInterrupt(states.falling)) {
		//Debug.Log("falling");
		SM.clearInterrupt();
		SM.setState(states.falling);
		landed = 0;
 		return(ops.opNext);
	} 
	if (checkLanded()) {
		SM.setState(states.onPlatform);
 		return(ops.opNext);
	}
	
	if (SM.newState() == false && checkWalls() && wjump == 3 ) {
		SM.setState(states.wallSliding);
 		return(ops.opNext);
	}
	
	if (SM.getInterrupt(states.falling) && checkFalling()) {
		//Debug.Log("APEX!");
		SM.setState(states.falling);
 		return(ops.opNext);
	}
	//pPhys.doFriction(playerJumpFriction);
	return(ops.opContinue);
}

function disabled()
{
	if (SM.newState() ) {
		//Debug.Log("OH GOD YOU SUCK");
		//playerSprite.Play("disabled");
		this.collider.isTrigger = true;	
		if (playerRigidbody.velocity.x != 0) 
		{
			playerRigidbody.velocity.x = 0;
		}
		canMove=false;
		Time.timeScale = 0.5;
	}
	 if (SM.getInterrupt(states.falling)) {
	  SM.setState(states.falling); 
	  Time.timeScale = 1;
	  return(ops.opNext);
	 }
	
	return(ops.opContinue);
}

function dead()
{

}
	
/*------------------------------------------------------------------------------------------------	
								END OF STATE MACHINE 
------------------------------------------------------------------------------------------------	
*/
function FixedUpdate() {
	//pPhys.doGravity();
	pPhys.tick();
	}

function Update () {
	
	movementThisStep = playerRigidbody.position - previousPosition;
//	var movementSqrMagnitude : float = movementThisStep.sqrMagnitude;
	previousPosition = playerRigidbody.position;
	SM.doStates();
	movePlayer();


}

function movePlayer () {
// var directionVector : Vector3 = Vector3.zero;
        // we assume that device is held parallel to the ground
        // and Home button is in the right hand
        
        // remap device acceleration axis to game coordinates:
        //  1) XY plane of the device is mapped onto XZ plane
        //  2) rotated 90 degrees around Y axis
     //  directionVector.x = Input.acceleration.x;
        //directionVector = Input.acceleration.x;
//Debug.Log(directionVector.x);        
        // clamp acceleration vector to unit sphere
//        if (directionVector.sqrMagnitude > 1)
  //          directionVector.Normalize();

 directionVector = new Vector3(Input.GetAxis("Horizontal"),0,0);

 
    if (directionVector != Vector3.zero) {
        var directionLength = directionVector.magnitude;
        directionVector = directionVector / directionLength;
        directionLength = Mathf.Min(1, directionLength);
        directionLength = directionLength * directionLength;
       // var scale:int;
        
        directionVector = directionVector * directionLength;
    }
    if (canMove == true && Time.timeScale > 0) {
//    if (directionVector.x < 0) { playerSprite.scale.x = -1;}
 //   if (directionVector.x >= 0) { playerSprite.scale.x = 1;}
        
    	//playerSprite.scale = Vector3(scale,1,1);
    	//pPhys.motionSetX(directionVector.x*speed);
			//pPhys.motionAdd(Vector3(directionVector.x*speed,0,0));
			
    }
/*
	if (canMove == true && Time.timeScale > 0) {
		if (Input.GetKey(KeyCode.LeftArrow)) { 
			playerSprite.scale = Vector3(-1,1,1);
			pPhys.motionAdd(Vector3(-.5 - speed * playerAcceleration,0,0));
		}
		if (Input.GetKey(KeyCode.RightArrow)) { 
			playerSprite.scale = Vector3(1,1,1);
			pPhys.motionAdd(Vector3(.5 + speed * playerAcceleration,0,0));
		}
		
	}*/
//	pPhys.doFriction(playerJumpFriction);
	
}

function OnCollisionEnter (collision : Collision) 
{
}

function OnCollisionExit (collision : Collision) 
{
}

function AnimationEventDelegate( sprite : tk2dAnimatedSprite, clip : tk2dSpriteAnimationClip, frame : tk2dSpriteAnimationFrame, frameNum : int)
{
	if(frame.eventInfo == "endRun")
	{
		SM.setInterrupt(states.jumping);
	} 
	if(frame.eventInfo == "endWallJump")
	{
		SM.setInterrupt(states.falling);
	} 
		if(frame.eventInfo == "endDisable")
	{
		SM.setInterrupt(states.falling);
		this.collider.isTrigger = false;
	} 
}

function jump()
{
	pPhys.motionAdd(Vector3.up* playerJump);
}

function checkLanded() : boolean {
	var isLanded : boolean;
	//wjump = false;
	var pos = playerRigidbody.position - Vector3(0,playerRigidbody.collider.bounds.extents.y,0) + Vector3(0,0.1,0);
	var dist = 0.1;
	isLanded = (Physics.Raycast(pos - Vector3(playerRigidbody.collider.bounds.extents.x,0,0), Vector3.down, dist , platformLayer) || Physics.Raycast(pos, Vector3.down, dist , platformLayer) || Physics.Raycast(pos + Vector3(playerRigidbody.collider.bounds.extents.x,0,0), Vector3.down, dist , platformLayer));
	return(isLanded);
}



function checkWalls() : int {

	isOnWall = 0;
	var pos = playerRigidbody.position;
	var dist = playerRigidbody.collider.bounds.extents.x + 0.1;
	/*
	if (playerRigidbody.velocity.x < 0) {
	side = "left";
	}
	if (playerRigidbody.velocity.x > 0) {
	side = "right";
	}
	*/ 
	//if (Physics.Raycast(pos, Vector3.left, dist , wallLayer)) { side = "left";}
	//if (Physics.Raycast(pos, Vector3.right, dist , wallLayer)) { side = "right";} 
	
		if (Physics.Raycast(pos, Vector3.left, dist , wallLayer)) {
			isOnWall = -1;
		}
		if (Physics.Raycast(pos, Vector3.right, dist , wallLayer)) {
			isOnWall = 1;
		 }
	 	
	return(isOnWall);
}



function checkFalling() : boolean {
	return(movementThisStep.y <= -0.0);
}

function OnDamage() : boolean {
	SM.setInterrupt(states.disabled);
	//Debug.Log("You got damaged");
}

function Slow() {
	
		
	//Debug.Log("SLOW!!");
	//Time.timeScale = 0.5;
	var timer : float = 2;
	
	if (timer > 0)
	{
		timer -= Time.deltaTime;
	}
	else if (timer <= 0)
	{
		//playerJump += 2;
		//Time.timeScale = 1.0;
	}
	
}

private var state;
private var statetest;
