#pragma strict

class StateMachine {

	private var currentState : states;
	private var nextState : states;
	private var previousState : states;
	private var defaultState : states;
	private var interrupts : states;
	
	private var operation : ops;
	private var stateTable : Hashtable;
	
	function StateMachine(defaultOp : ops, table : Hashtable) {
		operation = defaultOp;
		stateTable = table;
		defaultState = states.stDefault;
		previousState = defaultState;
		
	}
	
	function doStates() {
		var stateToDo : Function = stateTable[defaultState] as Function;
		
		switch (operation) {
		
			case ops.opDefault :
				stateToDo = stateTable[defaultState] as Function;
				currentState = defaultState;
				break;
				
			case ops.opContinue :
				previousState = currentState;
				stateToDo = stateTable[currentState] as Function;
				break;
				
			case ops.opNext :
				currentState = nextState;
				stateToDo = stateTable[nextState] as Function;
				
		}
		
		operation = stateToDo();
		
	}
	
	function getState() {
		return currentState;
	}
	
	function setState(stateName : states) {
		nextState = stateName;
	}
	
	function getPrevious() {
		return previousState;
	}
	function getNext() {
		return nextState;
	}
	
	function newState() {
		return(getPrevious() != getState());
	}
	
	function setInterrupt(stateName :states) {
		interrupts = stateName;
	}
	
	function getInterrupt(stateName : states) {
		//eventually set this to on in a bitwise flag enum
		return(interrupts == stateName);
	}
	
	function clearInterrupt() {
		//eventually set this to off in a bitwise flag enum
		interrupts = states.stNone ;
	}
};