#pragma strict

public class gamestate extends MonoBehaviour {

	//properties
	public static var instance:gamestate;

	
	
	public static var highScore:int;
	private var highScoreSave:int;
	private var nextLevel:String = "Level1";

	private var activeLevel:String;
	private var currentLevel:String;
	
	private var lives:int;
	private var kills:int;
	private var maxHP:int;
	private var hp:int;
	private var jumpPower:int;
	private var speed:int;
	private var toughness:int;
	private var score:int;
	private var busyMon:boolean;
	private var tmpOre:int;
	
	
	
	
	
	
	
	
	public function getLevel():String {return activeLevel;}
	
	public function getLives():int { return lives; }
	public function getHP():int { return hp; }
	public function getMaxHP():int { return maxHP; }
	
	public function getHighScore():int { highScore = PlayerPrefs.GetInt("High Score"); return highScore; }
	
	
	
	public function setLevel(newLevel:String) { activeLevel = newLevel;}
	public function setHP(newHP:int) { hp = newHP;}
	public function setMaxHP(newMaxHP:int):void { maxHP = newMaxHP;}
	
	public function setHighScore(newHighScore:int):void { highScore = newHighScore;}
	
	public function setLives(newLives:int):void { lives= newLives; }
	
	
	
	
public static function Instance():gamestate
	{
		

		
			return instance;
	}
function Awake() {
		 if (instance != null && instance != this)
        {
            Destroy(this);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
        activeLevel = "Level1";


}

	public function OnApplicationQuit():void
	{
		instance = null;
		PlayerPrefs.SetInt("High Score", highScore);
		//if (ore > PlayerPrefs.GetInt("Ore")) {PlayerPrefs.SetInt("Ore", ore);}	
	}
	
	public function startState():void
	{
		print("Creating a new game state");
							if (instance == null) {
				instance = new GameObject("gamestate").AddComponent(gamestate);
				DontDestroyOnLoad(instance);
				}

		
		activeLevel = "Intro";
		nextLevel = "Level1";
		
		Application.LoadLevel("Intro");
	}
	

	public function loadCurrent() {
		switch(nextLevel) {
			case "Level1":
				startGame1();
				break;
			case "Level2":
				startGame1();
				break;
			case "Level3":
				startGame2();
				break;
			case "Level4":
				startGame3();
				break;
			case "Level5":
				startGame4();
				break;
			case "victory":
				startGame5();
				break;
			default:
				startGame1();
				break;
			}
	}
		
	public function loadNext() {
		switch(nextLevel) {
			case "Level1":
				startGame1();
				break;
			case "Level2":
				startGame2();
				break;
			case "Level3":
				startGame3();
				break;
			case "Level4":
				startGame4();
				break;
			case "Level5":
				startGame5();
				break;
			case "victory":
				startState();
				break;

			}
	}
	
	
	
	public function startGame1():void
	{
		

		print("Starting Level 1");
		highScore = PlayerPrefs.GetInt("High Score");
		activeLevel = "Level1";
		nextLevel = "Level2";
		//activeSkin = 0;
//		grav = 1;
		lives = 1;
		score = 0;
		maxHP = 3;
		hp = 0;
	
		//ore = 0;
		//Time.timeScale = 0;
		Application.LoadLevel("Level1");
		//Time.timeScale = 0;
	}
	public function startGame2():void
	{
		print("Creating a new game state");
		highScore = PlayerPrefs.GetInt("High Score");
		activeLevel = "Level2";
		nextLevel = "Level3";
		
		lives = 1;
		
		//score;
		maxHP = 4;
		hp = 0;
		
		//ore = 0;
		Time.timeScale = 0;
		Application.LoadLevel("Level2");
	}
	public function startGame3():void
	{
		print("Creating a new game state");
		highScore = PlayerPrefs.GetInt("High Score");
		activeLevel = "Level3";
		nextLevel = "Level4";
		
		lives = 1;
		
		//score;
		maxHP = 5;
		hp = 0;
	
		//ore = 0;
		Time.timeScale = 0;
		Application.LoadLevel("Level3");
	}
		public function startGame4():void
	{
		print("Creating a new game state");
		highScore = PlayerPrefs.GetInt("High Score");
		activeLevel = "Level4";
		nextLevel = "Level5";
		
		lives = 1;
		
		//score;
		maxHP = 5;
		hp = 0;
		
		//ore = 0;
		Time.timeScale = 0;
		Application.LoadLevel("Level4");
	}	
		public function startGame5():void
	{
		print("Creating a new game state");
		highScore = PlayerPrefs.GetInt("High Score");
		activeLevel = "Level5";
		nextLevel = "victory";
		
		lives = 1;
		
		//score;
		maxHP = 5;
		hp = 0;
		
		//ore = 0;
		Time.timeScale = 0;
		Application.LoadLevel("Level5");
	}	
		
		public function gameOver():void
	{
		
		print("Creating a new game state");
		
		activeLevel = "gameover";
		Application.LoadLevel("gameover");
		
	}		
}