#pragma strict
//Compartmentalized Physics Class

class CPhysics {
	
	
	private var playerRigidbody : Rigidbody;
	
	private var gravity = 18; //gamestate.Instance().getGrav();
	private var friction;
	private var airFriction;
	
	private var newVelocity : Vector3;
	
	private var platformLayer : LayerMask;
	
	private var footingOffset : Vector3;
	
	private var addX : boolean = true;
	private var addY : boolean = true;
	
	private var previousVelocity : Vector3;
	
	function CPhysics ( trans : Transform, platformLayer : LayerMask) {
		
		this.playerRigidbody = (GameObject.FindGameObjectWithTag("Player")).GetComponent(Rigidbody);
		
		this.platformLayer = platformLayer;
		
		footingOffset = Vector3(playerRigidbody.collider.bounds.extents.x,0,0);
	}
	
	function doGravity() {
		newVelocity.y -= gravity;
	}
	
	function doFriction(friction : Vector3) {
		newVelocity.x *= friction.x;
		newVelocity.y *= friction.y;
		newVelocity.z *= friction.z;
	}
	
	function motionAdd(vel : Vector3) {
		newVelocity += vel;
		
	}
	
	function motionSetY(speed : float) {
		newVelocity.y = speed;
		addY = false;
		
	}
	function motionSetX(speed : float) {
		newVelocity.x = speed;
		addX = false;
		
	}

	
	function clampToObject() {
		////Debug.Log("clamping");
		var hitInfo : RaycastHit;
		if (Physics.Raycast(playerRigidbody.position - footingOffset, Vector3.down, hitInfo, 1, platformLayer) ||
			Physics.Raycast(playerRigidbody.position + footingOffset, Vector3.down, hitInfo, 1, platformLayer)) {
			
			playerRigidbody.position.y = hitInfo.point.y + playerRigidbody.collider.bounds.extents.y;
		}
	}
	
	function addPreviousVelocity (x : boolean, y : boolean) {
		
		if (x) {
			newVelocity.x += playerRigidbody.velocity.x;
		}
		
		if (y) { 
			newVelocity.y += playerRigidbody.velocity.y;
		}
		addX = true;
		addY = true;
	}
	
	function getPreviousVelocity() : Vector3 {
		return(newVelocity);
	}
	
	function tick() {
		previousVelocity = playerRigidbody.velocity;
		addPreviousVelocity(addX,addY);
		newVelocity.x = Mathf.Clamp(newVelocity.x, -12, 12);
		newVelocity.y = Mathf.Clamp(newVelocity.y, -8, 80);
		playerRigidbody.velocity = newVelocity;
		newVelocity = newVelocity.zero;
		
	}
	
}